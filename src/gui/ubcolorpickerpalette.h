#ifndef UBCOLORPICKERPALETTE_H
#define UBCOLORPICKERPALETTE_H
#include <QtGui>
#include "UBFloatingPalette.h"
#include "colorwheel.h"


class UBColorPickerPalette: public UBFloatingPalette
{
    Q_OBJECT;
public:
    UBColorPickerPalette(QColor color = QColor("red"));
    virtual ~UBColorPickerPalette();

    QSize sizeHint();

    ColorWheel *wheel() const;

private:
    ColorWheel* m_wheel;

};

#endif // UBCOLORPICKERPALETTE_H
