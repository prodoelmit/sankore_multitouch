#include "ubcolorpickerpalette.h"
#include <QGridLayout>
#include <core/UBApplication.h>

UBColorPickerPalette::UBColorPickerPalette(QColor color)
{
    new QGridLayout(this);
    m_wheel = new ColorWheel();
    MAKEVOTUMCOMPATIBLE(m_wheel);
    m_wheel->resize(QSize(200, 200));
    MAKEVOTUMCOMPATIBLE(this);
    layout()->addWidget(m_wheel);
}

UBColorPickerPalette::~UBColorPickerPalette()
{

}

QSize UBColorPickerPalette::sizeHint()
{
    return QSize(200, 200);

}

ColorWheel *UBColorPickerPalette::wheel() const
{
    return m_wheel;
}
