#ifndef UBCOLORINDICATOR_H
#define UBCOLORINDICATOR_H
#include <QToolBar>
#include <QMap>
#include <QToolButton>

#include <QWidget>

class UBColorIndicator: public QWidget
{
    Q_OBJECT;
public:
    UBColorIndicator(QToolBar *toolbar);

protected:
    void paintEvent(QPaintEvent *);
public slots:
    void updateColor(int id, const QColor& color);
private:
    QMap<int, QColor> m_colors;

    QMap<int, QToolButton*> m_buttons;
    QToolButton* mToolButton;


};

#endif // UBCOLORINDICATOR_H
