#include "UBColorIndicator.h"
#include "board/UBDrawingController.h"
#include "qdebug.h"
#include <QHBoxLayout>
#include <QPixmap>

UBColorIndicator::UBColorIndicator(QToolBar *toolbar)
    : QWidget(toolbar)
{
    mToolButton = qobject_cast<QToolButton*>(toolbar->layout()->itemAt(0)->widget());
    Q_ASSERT(mToolButton);
    QHBoxLayout* layout = new QHBoxLayout();
    this->setLayout(layout);

    m_buttons.insert(-1, new QToolButton());
    m_buttons.insert(1, new QToolButton());
    m_buttons.insert(2, new QToolButton());
    m_buttons.insert(3, new QToolButton());
    m_buttons.insert(4, new QToolButton());
    for (QMap<int, QToolButton*>::iterator it = m_buttons.begin(); it != m_buttons.end(); ++it) {
        QToolButton* tb = it.value();
        tb->setToolButtonStyle(Qt::ToolButtonIconOnly);
        if (it.key() == -1) {
            tb->setObjectName("ubButtonGroupLeft");
        } else if (it.key() == 4) {
            tb->setObjectName("ubButtonGroupRight");
        } else {
            tb->setObjectName("ubButtonGroupCenter");
        }
        layout->addWidget(tb);

    }

}

void UBColorIndicator::updateColor(int id, const QColor &color)
{
    if ( ! m_buttons.contains(id)) return;

    QToolButton* tb = m_buttons[id];

    QPixmap pixmap(32,32);
    pixmap.fill(color);
    QIcon icon(pixmap);
    tb->setIcon(icon);

}
void UBColorIndicator::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    QStyleOptionToolButton option;
    QPixmap emptyPixmap(32, 32);
    emptyPixmap.fill(Qt::transparent);
    QIcon emptyIcon(emptyPixmap);

    option.initFrom(mToolButton);

//    option.text = mLabel;
    option.font = mToolButton->font();

    int pointSize = mToolButton->font().pointSize();
    if (pointSize > 0)
        option.font.setPointSize(pointSize);
    else
    {
        int pixelSize = mToolButton->font().pixelSize();
        if (pixelSize > 0)
            option.font.setPixelSize(pixelSize);
    }

    option.rect = rect();
    option.icon = emptyIcon; // non null icon is required for style()->drawControl(QStyle::CE_ToolButtonLabel, ...) to work correctly
    option.iconSize = emptyPixmap.size();
//    option.toolButtonStyle = mDisplayLabel ? Qt::ToolButtonTextUnderIcon : Qt::ToolButtonIconOnly;

    style()->drawControl(QStyle::CE_ToolButtonLabel, &option, &painter, this);
}


